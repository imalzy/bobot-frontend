const colors = {
  primary: '#CF081E',
  secondary: '#0008C7',
  whites: '#FFFFFF',
  gray: '#b7b7bb',
};
  
export default colors;