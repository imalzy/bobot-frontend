import { gql, useMutation } from '@apollo/client';

// export const Login = gql`
//     mutation Login($tokenId:String!){
//         login(login: {token: $tokenId}){
//             accessToken,
//             expiresIn
//         }
//     }
// `

export const ADD_TODO = gql`
  mutation LoginMutation($token: String!) {
    login(login: {token: $token}) {
        accessToken,
        expiresIn
    }
  }
`;