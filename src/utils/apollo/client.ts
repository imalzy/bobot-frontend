import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

const link = createHttpLink({
  uri:'https://bobot.nias.dev/query',
  credentials: 'same-origin',
});

const authLink = setContext((_, { headers })=>{
  const token = localStorage.getItem('accessToken');

  return {
    headers:{
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  }
})

export const client = new ApolloClient({
  link : authLink.concat(link), 
  cache: new InMemoryCache(),
});