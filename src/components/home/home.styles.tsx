import { defaultCipherList } from 'constants'
import React from 'react'
import styled from 'styled-components'
import colors from '../../utils/colors/colors'

export const Wrapper = styled.div`
  background-color: ${colors.whites};
  color:white;
  max-width: 500px;
  width: 100%;
  min-height: 100vh;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color:#000000;
`

export const Navbar = styled.div`
    max-width: 480px;
    width: 100%;
    min-height: 10vh;
    margin: auto;
    display: flex;
    position: absolute;
    justify-content: space-between;
    align-items: center;
    top: 0px;
`

export const Title = styled.h2`
    margin-left: 10px;
    font-weight:700;
`

export const Logout = styled.h5`
    margin-right: 10px;
    color:${colors.primary} !important;
`

export const Description = styled.h5`
    margin: 0 !important;
`
export const ImageState = styled.img`
    width: 100%;
    max-width: 200px;
`

export const AddWallet = styled.button`
    position: absolute;
    font-size : 14px;
    font-weight: 600;
    width: 80%;
    height: 120px;
    top: 400px;
    background: rgba(18, 18, 29, 0.05);
    border-radius: 16px;
    border:0;
`;