import React from 'react'

import { Description, ImageState, Logout, Navbar, Title, Wrapper } from './home.styles';
import  AddWalletComponent  from '../addwallet/add-wallet.component';


const HomeComponent = () =>(
  <Wrapper>
    <Navbar>
      <Title>Your Wallet</Title>
      <Logout>Logout</Logout>
    </Navbar>
    <ImageState src="/assets/dompet.png" alt="State Home" />

    <Title style={{ margin: '0 !important' }}>You Don’t have any wallet</Title>
    <Description>Create one to track your expenses</Description>
    <AddWalletComponent>
      + <br /> Add Wallet
    </AddWalletComponent>
  </Wrapper>
);

export default HomeComponent;