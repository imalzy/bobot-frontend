import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Layout  from '../../layouts/container.component';
import HomeComponent from '../home/home.component';
import LoginComponent from '../login/login.component';

import './app.component.css';

const App: React.FC = () => (
  <Router>
    <Layout>
      <Route exact path="/" component={LoginComponent} />
      <Route exact path="/home" component={HomeComponent} />
    </Layout>
  </Router>
)

export default App;