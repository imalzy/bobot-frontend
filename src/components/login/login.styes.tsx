// eslint-disable-next-line import/no-extraneous-dependencies
import styled from 'styled-components'
import { GoogleLogin } from 'react-google-login'
import colors from '../../utils/colors/colors'

export const Wrapper = styled.div`
  background-color: ${colors.primary};
  color:white;
  max-width: 500px;
  width: 100%;
  min-height: 100vh;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
export const HeaderContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom:20%;
`
export const Title = styled.h1`
  font-size:  calc(25px + 6vmin);
  font-style: italic;
  font-weight: 700;
  margin-top: 30px;
  margin:0;
`
export const Description = styled.h2`
  font-size:  calc(15px + 2vmin);
  font-weight: normal !important;
  margin:0;
`
export const ImageState = styled.img`
    width: 100%;
    max-width: 200px;
`
export const ButtonGoogle = styled(GoogleLogin)`
  opacity: 1 !important;
  width: 90%;
  height: 56px;
  border-radius: 40px !important;
  overflow: hidden;
  display: flex !important;
  justify-content: center;
`
