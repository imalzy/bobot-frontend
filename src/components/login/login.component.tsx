/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-console */
import { useMutation } from '@apollo/client';
import React from 'react'
import { useHistory } from 'react-router-dom'
import { ADD_TODO } from '../../utils/graphql/mutation';
import { ButtonGoogle, Description, HeaderContent, ImageState, Title, Wrapper } from './login.styes'

const LoginComponent = () => {
  const history = useHistory();
  const clientId = '1068092152608-9i7o596ur7ripl1fi4tcrrvmgilu7h3c.apps.googleusercontent.com'
  const [LoginMutation, { data }] = useMutation(ADD_TODO);
  
  const responseGoogle = React.useCallback(async (response: any) => {
    const { tokenId } = response;
    await LoginMutation({
      variables: {
        token: tokenId,
      },
    });
  }, []);

  if (data){
    const { accessToken, expiresIn } = data.login;
    localStorage.setItem('accessToken', accessToken);
    localStorage.setItem('expiresIn', expiresIn);
    history.push('/home')
  }

  const onFailure = (err: any) => {
    console.log(err)
  }

  return (
    <Wrapper>
      <HeaderContent>
        <ImageState src="/assets/splashlogo.png" alt="State Login" />
        <Title>BOBOT</Title>
        <Description>Manage your expenses</Description>
      </HeaderContent>
      <ButtonGoogle
        clientId={clientId}
        buttonText="Continue with Google"
        onSuccess={responseGoogle}
        onFailure={onFailure}
        cookiePolicy="single_host_origin"
      />
    </Wrapper>
  )
}

export default LoginComponent
