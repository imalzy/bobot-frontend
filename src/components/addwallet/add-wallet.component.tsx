/* eslint-disable react/require-default-props */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'
import { Wallet } from './add-wallet.styles'

type BtnProps = {
  children?: React.ReactNode;
  onClick?:
  | ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void)
  | undefined;
  props?: any;
}

const AddWalletComponent = ({ onClick, children, ...props }: BtnProps)=>(
  <Wallet {...props} onClick={onClick}>
    {children}
  </Wallet>
)

export default AddWalletComponent;