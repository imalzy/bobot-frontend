import React from 'react';
import styled from 'styled-components';

export const Wallet = styled.button`
    position: relative;
    top: 60px;
    font-size : 14px;
    font-weight: 600;
    width: 80%;
    height: 120px;
    background: rgba(18, 18, 29, 0.05);
    border-radius: 16px;
    border:0;
`;