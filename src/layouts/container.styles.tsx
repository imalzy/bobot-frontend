import styled from 'styled-components';

export const Container = styled.section`
  max-width: 500px;
  width: 100%;
  height: 100vh;
  height: calc(var(--vh, 1vh) * 100);
  margin: auto;
`;