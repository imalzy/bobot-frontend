import React, { FC } from 'react';
import { Container } from './container.styles';

const Layout: FC = ({ children })  => (
  <Container>
    {children}
  </Container>
);

export default Layout;